# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
{
    'name': 'Account Invoice Billing Timeline',
    'name_de_DE': 'Fakturierung Rechnungsstellung aus Abrechnungsdaten Gültigkeitsdauer',
    'version': '2.2.0',
    'author': 'virtual things',
    'email': 'info@virtual-things.biz',
    'website': 'http://www.virtual-things.biz',
    'description': '''
    - Adds timeline features to 'Account Invoice Billing'
''',
    'description_de_DE': '''
    - Fügt die Merkmale der Gültigkeitsdauermodule für
      'Fakturierung Rechnungsstellung aus Abrechnungsdaten' hinzu.
''',
    'depends': [
        'account_invoice_billing',
        'account_product_rule',
        'account_timeline',
        'account_timeline_invoice',
    ],
    'xml': [
    ],
    'translation': [
        'locale/de_DE.po',
    ],
}
