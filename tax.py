# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from trytond.model import ModelView, ModelSQL
from trytond.pool import Pool


class RuleLine(ModelSQL, ModelView):
    _name = 'account.tax.rule.line'

    def match_hook(self, line, pattern, field):
        fiscalyear_obj = Pool().get('account.fiscalyear')

        res = super(RuleLine, self).match_hook(line, pattern, field)
        if res:
            # check for valid taxation_method
            if pattern.get('effective_date'):
                fiscalyear = fiscalyear_obj.get_fiscalyear(
                    date=pattern['effective_date'])
                if field == 'taxation_method':
                    if line['taxation_method'] != fiscalyear.taxation_method:
                        return False
        return res

RuleLine()
