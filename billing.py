#This file is part of Tryton. The COPYRIGHT file at the top level of
#this repository contains the full copyright notices and license terms.
from trytond.model import ModelView, ModelSQL, fields
from trytond.wizard import Wizard
from trytond.transaction import Transaction
from trytond.pool import Pool


class BillingLine(ModelSQL, ModelView):
    _name = 'account.invoice.billing_line'

    def __init__(self):
        super(BillingLine, self).__init__()
        self._error_messages.update({
            'no_account_for_billing_date':
                'Account "%s" does not exist at date %s!',
            })

    def _create_invoice(self, invoice_values, billing_line):
        account_obj = Pool().get('account.account')
        accounting_date = invoice_values['accounting_date']
        old_account_id = invoice_values['account']
        new_account = account_obj.get_account_by_date(old_account_id,
            accounting_date)
        if not new_account:
            old_account = account_obj.browse(old_account_id)
            self.raise_user_error(
                'no_account_for_billing_date', error_args=(
                    old_account.rec_name, accounting_date))
        invoice_values['account'] = new_account
        return super(BillingLine, self)._create_invoice(invoice_values,
            billing_line)

    def _get_invoice_line_billing(self, line, invoice_id, date):
        account_rule_obj = Pool().get('account.account.rule')

        res = super(BillingLine, self)._get_invoice_line_billing(line,
                invoice_id, date)
        taxes = res['taxes'][0][1] if res.get('taxes') else []
        res['account'] = account_rule_obj.get_account(
            'revenue', line.party, date, line.product, taxes)
        return res

    def _get_account_from_line(self, line):
        return False

    def _get_taxes_from_line(self, line):
        pool = Pool()
        tax_rule_obj = pool.get('account.tax.rule')
        invoice_line_obj = pool.get('account.invoice.line')
        res = []

        pattern = self._get_tax_rule_pattern(line, {})
        # Currently we handle only outgoing invoices for billing
        invoice_type = 'out_invoice'
        tax_rule = invoice_line_obj._get_tax_rule(line.party, invoice_type)
        ## Legacy for documentation
        ## For billing we use already product.tax_group instead of
        ## product.tax.group for pattern matching (issue2274)
        #for tax in line.product.customer_taxes_used:
        #    tax_ids = tax_rule_obj.apply(tax_rule, tax, pattern)
        #    if tax_ids:
        #        res.extend(tax_ids)
        #    continue
        #    res.append(tax.id)
        tax_ids = tax_rule_obj.apply(tax_rule, False, pattern)
        if tax_ids:
            res.extend(tax_ids)
        return res

    def _get_tax_rule_pattern(self, line, vals):
        pool = Pool()
        fiscalyear_obj = pool.get('account.fiscalyear')
        tax_group_obj = pool.get('account.tax.group')

        res = super(BillingLine, self)._get_tax_rule_pattern(line, vals)

        res['product_type_tax'] = line.product.type_tax.id
        res['effective_date'] = line.billing_date

        fiscalyear = fiscalyear_obj.get_fiscalyear(date=line.billing_date)
        res['taxation_method'] = fiscalyear.taxation_method

        # TODO: Replace this with a clean implementation of tax_group on
        # product (issue2274)
        tax_group_code = 'ust'
        tax_group_ids = tax_group_obj.search([('code', '=', tax_group_code)])
        tax_groups = tax_group_obj.browse(tax_group_ids)
        res['group'] = tax_groups[0].id
        return res

BillingLine()

